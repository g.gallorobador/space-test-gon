import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'

let camera, scene, renderer, controls;

let earth, clouds, pivotPoint, moon;

let numberOfLaps = 0;
const offsetLaps = 0.7;

const startButton = document.getElementById( 'startButton' );
startButton.addEventListener( 'click', init );

function init() {

    const overlay = document.getElementById( 'overlay' );
    overlay.remove();

    const container = document.getElementById( 'container' );
    
    // camera

    camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.z = 500;

    // scene

    scene = new THREE.Scene();
    const backgroundTexture = new THREE.TextureLoader().load('/textures/galaxy_starfield.png');
    scene.background = backgroundTexture;
    scene.fog = new THREE.Fog( 0x23272a, 0.5, 1700, 4000 );
    
    // light

    const spotLight = new THREE.SpotLight( 0xf0fdff, 2.3 );
    spotLight.position.set(-300, 400, 0);
    spotLight.castShadow = true;
    spotLight.shadow.bias = 0.000;
    spotLight.angle = Math.PI / 4;
    spotLight.penumbra = 0.05;
    spotLight.decay = 2;
    spotLight.distance = 1000;
    spotLight.shadow.camera.near = 1;
    spotLight.shadow.camera.far = 1000;
    spotLight.shadow.mapSize.width = 1024;
    spotLight.shadow.mapSize.height = 1024;
    camera.lookAt(scene.position);
    scene.add(spotLight);

    // hemilight

    const hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 1 );
    hemiLight.color.setHSL( 0.6, 1, 0.6 );
    hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
    hemiLight.position.set( 0, 500, 0 );
    scene.add(hemiLight);

    // renderer

    renderer = new THREE.WebGLRenderer({ antialias: false });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild( renderer.domElement );

    // controls

    controls = new OrbitControls(camera, renderer.domElement);

    // event Listener

    window.addEventListener('resize', onWindowResize, false);

    // audio

    let audioLoader = new THREE.AudioLoader();
    const listener = new THREE.AudioListener();
    camera.add( listener );

    // earth

    const earthTexture = new THREE.TextureLoader().load('/textures/earth_map.jpg');
    const bumpTexture = new THREE.TextureLoader().load('/textures/earth_bump.jpg');
    const earthGeometry = new THREE.SphereBufferGeometry(100, 32, 32);
    const earthMaterial = new THREE.MeshPhongMaterial({
        map: earthTexture,
        bumpMap: bumpTexture,
    });

    earth = new THREE.Mesh(earthGeometry, earthMaterial);

    earth.receiveShadow = true;
    earth.position.set(0, 0, 0);

    pivotPoint = new THREE.Object3D();
    earth.add(pivotPoint);

    scene.add(earth);

    // clouds
    const cloudsTexture = new THREE.TextureLoader().load('/textures/earthCloud.png');
    const cloudsGeometry = new THREE.SphereGeometry(104, 32, 32);
    const cloudsMaterial = new THREE.MeshPhongMaterial({
        map: cloudsTexture,
        transparent: true,
        opacity: 0.5,
    });

    clouds = new THREE.Mesh(cloudsGeometry, cloudsMaterial);

    scene.add(clouds);


    // moon

    const moonGeometry = new THREE.SphereBufferGeometry(30, 20, 20);
    const moonTexture = new THREE.TextureLoader().load('/textures/moon_texture.jpg')
    const moonMaterial = new THREE.MeshStandardMaterial({
        map: moonTexture,
    });

    moon = new THREE.Mesh(moonGeometry, moonMaterial);
    moon.position.set(300, 0, 0);


    pivotPoint.add(moon);

    // load audio 

    audioLoader.load( 'sounds/earth.mp3', function ( buffer ) {     

        const audioEarth = new THREE.PositionalAudio( listener );
        audioEarth.setBuffer( buffer );
        audioEarth.setLoop( true );
        audioEarth.setRefDistance(20);
        audioEarth.play();
        earth.add( audioEarth );
    } );

    audioLoader.load( 'sounds/moon_revolution.mp3', function ( buffer ) {     

        const audioMoon = new THREE.PositionalAudio( listener );
        audioMoon.setBuffer( buffer );
        audioMoon.setRefDistance(5);
        moon.add( audioMoon );
    } );

    animate();
}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);

}

function animate() {

    requestAnimationFrame(animate);
    controls.update();
    render();

}

function render() {

    // earth animation

    earth.rotation.y += 0.003;

    // clouds animation

    clouds.rotation.y += 0.0025;
    
    // moon animation

    moon.rotation.y += 0.01;
    pivotPoint.rotation.y += 0.001;

    // moon audio timing

    if (pivotPoint.rotation.y > offsetLaps + numberOfLaps) {
        const audio = moon.children[ 0 ];
        audio.play();
        numberOfLaps++;
    };

    renderer.render(scene, camera);
}