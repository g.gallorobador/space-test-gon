# Space Test

Once you have managed to open the website on your browser, click on the "Play" button. You will see the Earth and the Moon rotating in space. You can move around using the mouse, zoom in and out, and move sideways with the right click.

Both objects play a sound with 3D audio spatialization. Notice how the Earth plays a constant sound, while the Moon will produce its sound every time it crosses a certain point in its orbit. Move around to test the audio spatialization!

## Setup

This is a simple setup for three.js using a webpack starter by Bruno Simon of https://threejs-journey.xyz/

First of all, download [Node.js](https://nodejs.org/en/download/).

After that, clone this repository, open a terminal (for example, directly in VS Code), do cd to the repository folder and run the following commands:

``` bash
# Install dependencies
npm install

# Run the local server
npm run dev
